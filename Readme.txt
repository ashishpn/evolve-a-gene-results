EvolveAgene4 is a coding sequence evolution simulation program.
It simulates evolution by base substitutions, insertions, deletions 
and recombination events.

It does not use a mathematical model of evolution processes, instead
it used an even older model: mutation and selection.  The mutational spectrum 
(probability of each base substitution event) is the well studied E coli mutational sepctrum.

Selection parameters are set by the user by setting the relative probabilities of
accepting amino acid substituting base changes relative to silent mutations, 
but frame shifts and nonsense mutation are always rejected.


EvolveAGene4 is an upgrade of EvolveAGene3 ( Hall, B. G. 2008  Simulating DNA coding 
sequence evolution with EvolveAGene 3.  Mol. Biol. Evol.  25: 688-695.) that incorporates
recombination.

Please read the EvolveAGene4 manual for details