#!/usr/bin/env python3


import os
import sys
import shutil


in_file_name = sys.argv[1]
taxa_lens = [32]
brlens = [5, 10, 15, 20]

for tl in taxa_lens:
    for bl in brlens:
        dirname = "{}_taxa_{}_brlen".format(tl, bl)
        try:
            os.mkdir(dirname)
        except FileExistsError:
            pass
        shutil.copy(in_file_name, dirname)
