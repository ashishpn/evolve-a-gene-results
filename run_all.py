#!/usr/bin/env python3

import sys
import subprocess


in_file_name = sys.argv[1]
dirs = subprocess.run(["find", ".", "-type", "d"], stdout=subprocess.PIPE)\
        .stdout.decode("utf-8").split()
dirs = [d for d in dirs if "taxa" in d]

for d in dirs:
    name = d[2:].split("_")
    ntaxa, brlen = name[0], name[2]

    p = subprocess.run(["./EvolveAGene4",
                        "-f", d[2:]+"/"+in_file_name,
                        "-n", ntaxa,
                        "-b", brlen,
                        "-a", "0.1"])
    print(p)
