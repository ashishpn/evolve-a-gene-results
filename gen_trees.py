#!/usr/bin/env python3

import os
import sys
import platform


from Bio.Align.Applications import ClustalwCommandline
from Bio.Emboss.Applications import FNeighborCommandline,\
    FDNADistCommandline, FDNAParsCommandline, FTreeDistCommandline


in_file_prefix = sys.argv[1]

dirs = [d for d in os.listdir(os.getcwd()) if "taxa" in d]

if platform.system() == "Darwin":
    clustal_exe = "clustalw2"
else:
    clustal_exe = "clustalw2-linux"

for d in dirs:
    seq_file_name_tail = "{}_Unaligned.FASTA".format(in_file_prefix)
    seq_file_name = os.path.join(d, seq_file_name_tail)

    if (os.path.exists(seq_file_name)):
        print("Starting clustalw2 on {}".format(seq_file_name))
        clustal_instance = ClustalwCommandline(
            "./"+clustal_exe,
            infile=seq_file_name,
            outfile=seq_file_name_tail + "_clustalw2_output",
            output="fasta")
        out, err = clustal_instance()
        print(out)
        print(err)

        phylip_dnadist = FDNADistCommandline()
        phylip_neighbour = FNeighborCommandline()

        phylip_dnaparse = FDNAParsCommandline("./dnapars")
